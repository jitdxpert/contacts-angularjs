import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css'],
  providers: [
    AuthService
  ]
})
export class StatusComponent implements OnInit {

  isLogged: boolean = false;
  userData: any;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.isLogged = this.auth.isLoggedIn();

    const token = localStorage.getItem('token');
    this.auth.ensureAuthenticated(token).subscribe(
      result => {
        this.userData = result.data;
      },
      error => {
        console.log(error);
      }
    );
  }

}
