import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from '../../services/toaster/toaster.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [
    AuthService,
    ToasterService
  ]
})
export class NavbarComponent implements OnInit {

  isMenuShow: boolean = false;
  isDropdownShow: boolean = false;
  isLogged: boolean = false;
  userData: any;

  constructor(private router: Router, private auth: AuthService, private toast: ToasterService){ }

  ngOnInit(){
    this.isLogged = this.auth.isLoggedIn();

    const token = localStorage.getItem('token');
    this.auth.ensureAuthenticated(token).subscribe(
      result => {
        this.userData = result.data;
      },
      error => {
        console.log(error);
      }
    );
  }

  toggleMobileMenu(){
    this.isMenuShow = !this.isMenuShow;
  }

  toggleDropdownMenu(){
    this.isDropdownShow = !this.isDropdownShow;
  }

  logout() {
    this.auth.logout().subscribe(
      result => {
        this.isLogged = false;
        this.toast.showSuccess(result.message);
        localStorage.removeItem('token');
        this.router.navigateByUrl('/');
      },
      error => {
        console.log(error);
      }
    )
  }

}
