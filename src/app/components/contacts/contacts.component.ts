import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../services/contact/contact.service';
import { ContactSchema } from '../../services/contact/contact.schema';
import { ToasterService } from '../../services/toaster/toaster.service';


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
  providers: [
    ContactService,
    ToasterService
  ]
})
export class ContactsComponent implements OnInit{

  contacts: ContactSchema[];
  first_name: string;
  last_name: string;
  phone: string;

  constructor(private contactService: ContactService, private toast: ToasterService){

  }

  addContact(){
    const newContact: ContactSchema = {
      first_name: this.first_name,
      last_name: this.last_name,
      phone: this.phone
    };
    this.contactService.addContact(newContact).subscribe(contact => {
      if(contact.code == 'success'){
        this.ngOnInit();
        this.first_name = '';
        this.last_name = '';
        this.phone = '';
        this.toast.showSuccess(contact.message);
      }else{
        this.toast.showError(contact.message);
      }
    });
  }

  deleteContact(id:any){
    let contacts = this.contacts;
    this.contactService.deleteContact(id).subscribe(data => {
      if(data.n == 1){
        this.toast.showSuccess('Contact deleted successfully.');
        this.ngOnInit();
      }
    })
  }

  ngOnInit(){
    this.contactService.getContacts().subscribe(contacts => {
      this.contacts = contacts;
    });
  }

}
