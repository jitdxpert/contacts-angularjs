import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { LoginSchema } from '../../services/auth/auth.schema';
import { ToasterService } from '../../services/toaster/toaster.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [
    AuthService,
    ToasterService
  ]
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;

  constructor(private router: Router, private auth: AuthService, private toast: ToasterService) {}

  ngOnInit() {

  }

  onLogin() {
    const user: LoginSchema = {
      email: this.email,
      password: this.password
    };
    this.auth.login(user).subscribe(
      result => {
        this.email = null;
        this.password = null;
        this.toast.showSuccess(result.message);
        localStorage.setItem('token', result.token);
        this.router.navigateByUrl('/status');
      },
      error => {
        this.toast.showError(error._body);
      }
    );
  }

}
