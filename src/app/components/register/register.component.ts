import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { RegisterSchema } from '../../services/auth/auth.schema';
import { ToasterService } from '../../services/toaster/toaster.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [
    AuthService,
    ToasterService
  ]
})
export class RegisterComponent implements OnInit {

  first_name: string;
  last_name: string;
  email: string;
  password: string;

  constructor(private router: Router, private auth: AuthService, private toast: ToasterService) {}

  ngOnInit() {

  }

  onRegister() {
    const newUser: RegisterSchema = {
      first_name: this.first_name,
      last_name: this.last_name,
      email: this.email,
      password: this.password
    };
    this.auth.register(newUser).subscribe(
      result => {
        this.first_name = null;
        this.last_name = null;
        this.email = null;
        this.password = null;
        this.toast.showSuccess(result.message);
        localStorage.setItem('token', result.token);
        this.router.navigateByUrl('/status');
      },
      error => {
        this.toast.showError(error._body);
      }
    );
  }

}
