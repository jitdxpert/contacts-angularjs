import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { RegisterSchema, LoginSchema } from './auth.schema';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  constructor(private http: Http) {

  }

  // login user
  login(user: LoginSchema) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:4100/auth/login', user, {headers: headers}).map(res => {
      return res.json();
    });
  }

  // register user
  register(newUser: RegisterSchema) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:4100/auth/register', newUser, {headers: headers}).map(res => {
      return res.json();
    });
  }

  ensureAuthenticated(token) {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'x-access-token': token
    });
    return this.http.get('http://localhost:4100/auth/me', {headers: headers}).map(res => {
      return res.json();
    });
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    if(token){
      return true;
    }else{
      return false;
    }
  }

  logout() {
    return this.http.get('http://localhost:4100/auth/logout').map(res => {
      return res.json();
    });
  }

}
