export class RegisterSchema {

  _id?: string;
  first_name: string;
  last_name: string;
  email: string;
  password: string;

};

export class LoginSchema {

  email: string;
  password: string;

};
