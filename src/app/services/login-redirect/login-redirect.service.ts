import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class LoginRedirectService implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(): boolean {
    const token = localStorage.getItem('token');
    if(token){
      this.router.navigateByUrl('/status');
      return false;
    }else{
      return true;
    }
  }

}
