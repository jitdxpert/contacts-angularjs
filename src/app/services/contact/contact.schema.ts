export class ContactSchema {

  _id?: string;
  first_name: string;
  last_name: string;
  phone: string;

};
