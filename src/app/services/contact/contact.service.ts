import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ContactSchema } from './contact.schema';
import 'rxjs/add/operator/map';

@Injectable()
export class ContactService{

  constructor(private http: Http) {

  }

  // getting contacts
  getContacts(){
    return this.http.get('http://localhost:4100/api/contacts').map(res => res.json());
  }

  // adding contact
  addContact(newContact: ContactSchema){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:4100/api/contact', newContact, {headers: headers}).map(res => res.json());
  }

  // deleting contact
  deleteContact(id){
    return this.http.delete('http://localhost:4100/api/contact/'+id).map(res => res.json());
  }

}
